﻿using UnityEngine;

public static class MyExtensions
{
    public static Quaternion Hermite(this Quaternion q, Quaternion q2, float smooth)
    {
		return new Quaternion(
			Mathf.Lerp(q.x, q2.x, Hermite(smooth)),
			Mathf.Lerp(q.y, q2.y, Hermite(smooth)),
			Mathf.Lerp(q.z, q2.z, Hermite(smooth)),
			Mathf.Lerp(q.w, q2.w, Hermite(smooth))
		);
    }

	public static Vector3 Hermite(this Vector3 q, Vector3 q2, float smooth)
    {
		return new Vector3(
			Mathf.Lerp(q.x, q2.x, Hermite(smooth)),
			Mathf.Lerp(q.y, q2.y, Hermite(smooth)),
			Mathf.Lerp(q.z, q2.z, Hermite(smooth))
		);
    }

    public static float Hermite(float f)
    {
        return (3 * f * f) - (2 * f * f * f);
    }

	public static int mod(int a, int b)
	{
		return ((a % b) + b) % b;
	}

	public static float Remap (this float value, float oldFrom, float oldTo, float newFrom, float newTo) 
	{
    	return (value - oldFrom) / (oldTo - oldFrom) * (newTo - newFrom) + newFrom;
	}

	
}