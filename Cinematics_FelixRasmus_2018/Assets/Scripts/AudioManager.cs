﻿using UnityEngine;

public static class AudioManager  {

	public static uint PostEvent(string eventName, GameObject go)
	{
		return AkSoundEngine.PostEvent(eventName, go);
	}

	public static void SetRTPCValue(string rtpcName, float rtpcValue)
	{
		AkSoundEngine.SetRTPCValue(rtpcName, rtpcValue);
	}

	public static void SetRTPCValue(string rtpcName, float rtpcValue, GameObject go = null)
	{
		AkSoundEngine.SetRTPCValue(rtpcName, rtpcValue, go);
	}

	public static void SetSwitch(string switchGroup, string switchPostion, GameObject go)
	{
		AkSoundEngine.SetSwitch(switchGroup, switchPostion, go);
	}

	public static void SetState(string stateGroup, string statePosition)
	{
		AkSoundEngine.SetState(stateGroup, statePosition);
	}	
}
