﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PhysicsExtension 
{

   
	public static float BezierCast(Vector3 origin, Vector3 gravityForce, Vector3 currentVel, int resolution, float maxDistance)
	{
		//Initializing values
		Vector3 currentPos = origin;
		Vector3 hypoPos = origin;
		Vector3 simVelocity = currentVel;
		Vector3 hypoVelocity= simVelocity * Time.fixedDeltaTime / (resolution * resolution);
		float totalLength = 0;

		while(!Physics.Raycast(currentPos, hypoVelocity, hypoVelocity.magnitude) && totalLength < maxDistance)
		{
			totalLength += hypoVelocity.magnitude;
			simVelocity = simVelocity + ((gravityForce * Time.fixedDeltaTime)/ (resolution * resolution));
			hypoVelocity = simVelocity * Time.fixedDeltaTime / (resolution * resolution);
			currentPos = hypoPos;
			hypoPos = currentPos + hypoVelocity;
			//Debug.DrawRay(currentPos, hypoPos - currentPos, Color.blue, 5f);
		}
		return totalLength;
	}

	public static float BezierCast(Vector3 origin, Vector3 gravityForce, Vector3 currentVel, int resolution, float maxDistance, out float time)
	{
		//Initializing values
		Vector3 currentPos = origin;
		Vector3 hypoPos = origin;
		Vector3 simVelocity = currentVel;
		Vector3 hypoVelocity= simVelocity * Time.fixedDeltaTime / (resolution * resolution);
		float totalLength = 0;
		float totalTime = 0;

		while(!Physics.Raycast(currentPos, hypoVelocity, hypoVelocity.magnitude) && totalLength < maxDistance)
		{
			totalLength += hypoVelocity.magnitude;
			totalTime += Time.fixedDeltaTime / resolution;
			simVelocity = simVelocity + ((gravityForce * Time.fixedDeltaTime)/ (resolution * resolution));
			hypoVelocity = simVelocity * Time.fixedDeltaTime / (resolution * resolution);
			currentPos = hypoPos;
			hypoPos = currentPos + hypoVelocity;
			//Debug.DrawRay(currentPos, hypoPos - currentPos, Color.blue, 5f);
		}

		time = totalTime;
		return totalLength;
	}

	public static float BezierCast(Vector3 origin, Vector3 gravityForce, Vector3 currentVel, int resolution, float maxDistance, out List<Vector3> points)
	{
		//Initializing values
		Vector3 currentPos = origin;
		Vector3 hypoPos = origin;
		Vector3 simVelocity = currentVel;
		Vector3 hypoVelocity= simVelocity * Time.fixedDeltaTime / (resolution * resolution);
		float totalLength = 0;

		List<Vector3> localPoints = new List<Vector3>();

		while(!Physics.Raycast(currentPos, hypoVelocity, hypoVelocity.magnitude) && totalLength < maxDistance)
		{
			localPoints.Add(hypoPos);
			totalLength += hypoVelocity.magnitude;
			simVelocity = simVelocity + ((gravityForce * Time.fixedDeltaTime)/ (resolution * resolution));
			hypoVelocity = simVelocity * Time.fixedDeltaTime / (resolution * resolution);
			currentPos = hypoPos;
			hypoPos = currentPos + hypoVelocity;
		}
		points = localPoints;
		return totalLength;
	}
}
