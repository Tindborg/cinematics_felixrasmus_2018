﻿using UnityEngine;
using System.Collections;

public class RisingFloor : MonoBehaviour
{
	[SerializeField]
	private Transform _Platform;

	[SerializeField]
	private float _Distance = 1f;
	[SerializeField]
	private float _RisingSpeed = 3f;

	private Vector3 _positionDown;
	private Vector3 _positionUp;

	private bool _rising = false;
	private float _progression = 0f;

	private int _entitiesInCollider = 0;

	//
	
	private void Awake()
	{
		_positionDown = _Platform.position;
		_positionUp = _positionDown + new Vector3(0, _Distance, 0);
	}

	private void OnTriggerEnter(Collider other)
	{
		_entitiesInCollider ++;

		if (!_rising)
		{
			_rising = true;

			StopAllCoroutines();
			StartCoroutine(Rise());
		}
	}

	private void OnTriggerExit(Collider other)
	{
		_entitiesInCollider --;

		if (_entitiesInCollider <= 0)
		{
			_entitiesInCollider = 0;

			_rising = false;

			StopAllCoroutines();
			StartCoroutine(Lower());
		}
	}

	IEnumerator Rise()
	{
		while (_rising)
		{
			_progression += _RisingSpeed * Time.deltaTime;

			if (_progression >= 1f)
			{
				_progression = 1f;

				_rising = false;
			}

			UpdatePosition(_progression);

			yield return null;
		}
	}

	IEnumerator Lower()
	{
		while (_progression > 0)
		{
			_progression -= _RisingSpeed * Time.deltaTime;

			UpdatePosition(_progression);

			if (_progression <= 0)
			{
				StopAllCoroutines();
			}

			yield return null;
		}
	}

	private void UpdatePosition(float percentage)
	{
		// _Platform.position = Vector3.Lerp(_positionDown, _positionUp, _progression);
		float sinC = Mathf.Sin(Mathf.Lerp(Mathf.PI, Mathf.PI * 0.5f, percentage));

		// _Platform.position = Vector3.Lerp(_positionDown, _positionUp, _progression);
		_Platform.position = Vector3.Lerp(_positionDown, _positionUp, sinC);
	}

}
