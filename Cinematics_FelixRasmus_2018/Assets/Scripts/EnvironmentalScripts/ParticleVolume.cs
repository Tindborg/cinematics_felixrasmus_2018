﻿using UnityEngine;

public class ParticleVolume : MonoBehaviour
{

	[SerializeField]
	private GameObject _ParticleSystem;

	private void OnTriggerEnter(Collider other)
	{
		_ParticleSystem.SetActive(true);
	}

	private void OnTriggerExit(Collider other)
	{
		_ParticleSystem.SetActive(false);
	}

}
