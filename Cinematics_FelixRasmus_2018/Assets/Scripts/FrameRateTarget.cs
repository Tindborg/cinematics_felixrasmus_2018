﻿using UnityEngine;

public class FrameRateTarget : MonoBehaviour 
{
	private void Awake () 
	{
     QualitySettings.vSyncCount = 1;
     Application.targetFrameRate = 60;
	}
}
