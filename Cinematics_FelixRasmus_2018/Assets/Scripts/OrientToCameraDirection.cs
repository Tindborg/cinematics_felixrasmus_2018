﻿using UnityEngine;

public class OrientToCameraDirection : MonoBehaviour
{

	[SerializeField]
	private Transform _CameraTransform;

	private void LateUpdate()
	{
		// transform.rotation = Quaternion.LookRotation(Vector3.Scale((transform.position - _CameraTransform.position), Vector3.up).normalized, Vector3.up);
		transform.rotation = _CameraTransform.rotation;
		transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f);
	}

}
