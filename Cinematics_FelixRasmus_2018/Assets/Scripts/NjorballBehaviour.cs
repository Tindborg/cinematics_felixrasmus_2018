﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class NjorballBehaviour : MonoBehaviour
{

	[SerializeField]
	private float _UpForceAdditive = 10f;

	[SerializeField]
	private float _Gravity = 4.9f;

	private Rigidbody _rb;

	private void Awake()
	{
		_rb = GetComponent<Rigidbody>();
	}

	private void FixedUpdate()
	{
		_rb.AddForce(Vector3.down * _Gravity);
	}
	
	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			_rb.AddForce(Vector3.up * _UpForceAdditive, ForceMode.Impulse);
		}
	}

}
