﻿using UnityEngine;

public class PropagateSimpleParticleMaterial : ColorSpecificPropagator
{

	[SerializeField]
	private Material[] _Materials = new Material[]{};
	
	public override void Propagate(int colorId)
	{
		ParticleSystemRenderer pr = (ParticleSystemRenderer)GetComponent<Renderer>();

		pr.material = _Materials[colorId];
	}

}
