﻿using UnityEngine;

public class PropagateVertexStreamGradient : ColorSpecificPropagator
{

	[SerializeField]
	private Gradient[] _Gradient;
	
	public override void Propagate(int colorId)
	{
		GetComponent<ParticleSystem>().customData.SetColor(ParticleSystemCustomData.Custom1, _Gradient[colorId]);
	}

}
