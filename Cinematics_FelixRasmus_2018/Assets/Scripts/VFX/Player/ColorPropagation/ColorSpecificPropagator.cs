﻿using UnityEngine;

public abstract class ColorSpecificPropagator : MonoBehaviour
{
	
	public virtual void Propagate(int colorId){}

}
