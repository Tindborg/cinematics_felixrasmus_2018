﻿using UnityEngine;

public class PropagateParticleColorOverLifetime : ColorSpecificPropagator
{

	[SerializeField]
	private Gradient[] _Gradients;
	
	public override void Propagate(int colorId)
	{
		var col = GetComponent<ParticleSystem>().colorOverLifetime;

		col.color = _Gradients[colorId];
	}

}
