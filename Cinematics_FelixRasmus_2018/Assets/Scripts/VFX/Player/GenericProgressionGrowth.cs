﻿using UnityEngine;

public class GenericProgressionGrowth : MonoBehaviour
{
	
	[Tooltip("How much normalized progression increments per second")]
	[SerializeField]
	private float _ProgressionSpeed = 1f;

	private float _progress = 0f;

	private Material _material;


	// [SerializeField]
	// private float _DBDisplacementPower = 0.03f;
	// [SerializeField]
	// private float _DBVertexOffsetPower = 0.03f;
	// [SerializeField]
	// private float _DBScaleMax = 0.03f;


	private void Awake()
	{
		_material = GetComponent<Renderer>().material;
	}

	private void OnEnable()
	{
		_progress = 0f;

		// _material.SetFloat("_DisplacementPower", _DBDisplacementPower);
		// _material.SetFloat("_VertexOffsetPower", _DBVertexOffsetPower);
		// _material.SetFloat("_ScaleMax", _DBScaleMax);
	}

	private void Update()
	{
		_progress += _ProgressionSpeed * Time.deltaTime;

		if (_progress >= 1f)
		{
			gameObject.SetActive(false);
		} else {
			_material.SetFloat("_Progression", _progress);
		}
	}

	public void ForceRestart()
	{
		_progress = 0f;

		_material.SetFloat("_Progression", 0f);
	}

}
