﻿using UnityEngine;

// Don't use!

public class BallAnimationLocal : MonoBehaviour
{
	
	[SerializeField]
	private Rigidbody _RB;

	[SerializeField]
	private float _Multiplier = 3f;

	private void LateUpdate()
	{
		transform.Rotate(Vector3.right, _RB.velocity.magnitude * Time.deltaTime * _Multiplier, Space.Self);
	}

}
