﻿
using System.Collections;
using UnityEngine;

public class LegAnimation : MonoBehaviour
{

	//

	[SerializeField]
	private float _LegAnimSpeedMin = 0f;
	[SerializeField]
	private float _LegAnimSpeedMax = 3f;

	[SerializeField]
	private float _BallAnimSpeedMin = 0f;
	[SerializeField]
	private float _BallAnimSpeedMax = 5f;

	[SerializeField]
	private float _JumpLegSpeed = 0.8f;

	[SerializeField]
	private float _LegModeTopSpeed = 80f;

	[SerializeField]
	private float _BallModeTopSpeed = 100f;

	[SerializeField]
	private float _SpinForAnimation = 360f;

	[SerializeField]
	private Rigidbody _PhysicsRB;

	[SerializeField]
	private NjorbVFXManager _VFXManager;

	private Animator _Animator;
	private bool _dustPlaying = false;
	private bool _driftSparksPlaying = false;

	//

	[Space]

	[SerializeField]
	private float _RaycastDistance = 1.5f;
	[SerializeField]
	private LayerMask _RaycastLayer;

	[Space]
	[SerializeField]
	private GameObject _SoundGO;

	[Space]
	[SerializeField]
	private AnimationCurve _LandCurve;

	private float _soundSpeed;
	private int _spinCounter;
	private Quaternion _currentRotation = Quaternion.identity;
	private RaycastHit _hit;
	private bool _airTimeTimerRunning = false;


	public bool Grounded = false;
	public bool _moveSoundPlaying = true;

	private void Awake()
	{
		_Animator = GetComponent<Animator>();
	}

	private void OnDisable()
	{
		AudioManager.PostEvent("Stop_Njorb_Move", _SoundGO);
	}
	
	private void OnEnable()
	{
		AudioManager.SetSwitch("Form", "Leg", _SoundGO);
	}

	private void LateUpdate()
	{
		if(_Animator.GetBool("ballMode"))
		{
			if (Physics.Raycast(transform.parent.position, -Vector3.up, _RaycastDistance, _RaycastLayer))
			{
				float AnimSpeed = Mathf.LerpUnclamped(_BallAnimSpeedMin, _BallAnimSpeedMax, _PhysicsRB.velocity.magnitude/_BallModeTopSpeed);
				_Animator.SetFloat("Speed", AnimSpeed);
			} else 
			{
				float AnimSpeed = Mathf.Lerp(_BallAnimSpeedMin, _BallAnimSpeedMax, 0.25f);
				_Animator.SetFloat("Speed", AnimSpeed);
			}
			if(_dustPlaying)
			{
				_VFXManager.StopDustWalk();
				_dustPlaying = false;
			}
			if(_driftSparksPlaying)
			{
				_VFXManager.StopDrift();
				_driftSparksPlaying = false;
				// AudioManager.PostEvent("Stop_Njorb_Skid", _SoundGO);
				if(_Animator.GetBool("ballMode"))
				{
					AudioManager.SetSwitch("Form", "Ball", _SoundGO);
				} else 
				{
					AudioManager.SetSwitch("Form", "Leg", _SoundGO);
				}
			}

		} else
		{
			if (Physics.Raycast(transform.parent.position, -transform.up, out _hit, _RaycastDistance, _RaycastLayer))
			{
				Grounded = true;
				float newSpeed = Mathf.LerpUnclamped(_LegAnimSpeedMin, _LegAnimSpeedMax, _PhysicsRB.velocity.magnitude/_LegModeTopSpeed);

				_Animator.SetFloat("Speed", newSpeed);

				UpdateSound(newSpeed);

				if(newSpeed < 0.05f)
				{
					_Animator.SetBool("moving", false);
				} else 
				{
					_Animator.SetBool("moving", true);
				}

				if(!_dustPlaying)
				{
					_VFXManager.StartDustWalk();
					_dustPlaying = true;
				}

				_Animator.SetBool("Grounded", true);

				if(_Animator.GetBool("Grappling"))
				{
					transform.rotation= Quaternion.FromToRotation(transform.up, _hit.normal) * transform.rotation;
					if(!_driftSparksPlaying)
					{
						_driftSparksPlaying = true;
						//AudioManager.PostEvent("Play_Njorb_Skid", _SoundGO);
						AudioManager.SetSwitch("Form", "Skid", _SoundGO);
						if(transform.InverseTransformVector(_PhysicsRB.velocity).x > 0)
						{
							_Animator.SetBool("GrappleLeft", false);
							_VFXManager.StartDrift(true);
						} else 
						{
							_Animator.SetBool("GrappleLeft", true);
							_VFXManager.StartDrift(false);
						}
					}
					if(_dustPlaying)
					{
						_VFXManager.StopDustWalk();
						_dustPlaying = false;
					}
				} else 
				{
					if(_driftSparksPlaying)
					{
						_VFXManager.StopDrift();
						_driftSparksPlaying = false;
						// AudioManager.PostEvent("Stop_Njorb_Skid", _SoundGO);]
						if(_Animator.GetBool("ballMode"))
						{
							AudioManager.SetSwitch("Form", "Ball", _SoundGO);
						} else 
						{
							AudioManager.SetSwitch("Form", "Leg", _SoundGO);
						}
						
					}
				}
	

			} else {
				_Animator.SetFloat("Speed", _JumpLegSpeed);
				_Animator.SetBool("Grounded", false);

				Grounded = false;

				if(_dustPlaying)
				{
					_VFXManager.StopDustWalk();
					_dustPlaying = false;
				}
				if(_driftSparksPlaying)
				{
					_VFXManager.StopDrift();
					_driftSparksPlaying = false;
					//AudioManager.PostEvent("Stop_Njorb_Skid", _SoundGO);
					if(_Animator.GetBool("ballMode"))
					{
						AudioManager.SetSwitch("Form", "Ball", _SoundGO);
					} else 
					{
						AudioManager.SetSwitch("Form", "Leg", _SoundGO);
					}
				}
			}
		}
		if(!Grounded)
		{
			AirTimeGroundedCheck();
		}
	}

	private void FixedUpdate()
	{
		if(Quaternion.Angle(transform.rotation, _currentRotation) > _SpinForAnimation * Time.fixedDeltaTime && _PhysicsRB.velocity.magnitude < 20f)
		{
			_spinCounter ++;
		} else {
			_spinCounter -= 2;
		}
		_spinCounter = Mathf.Clamp(_spinCounter, 0, 30);

		_currentRotation = transform.rotation;
		
		if(_spinCounter >= 15 && !_Animator.GetBool("ballMode"))
		{
			_Animator.SetBool("Spinning", true);
		} else {
			_Animator.SetBool("Spinning", false);
		}
	}


	private void UpdateSound(float speed)
	{
		float speedRPTC = _PhysicsRB.velocity.magnitude / _LegModeTopSpeed;
		_soundSpeed = Mathf.Clamp(speedRPTC * 0.9f, 0, 1);
		AudioManager.SetRTPCValue("Njorb_Speed", _soundSpeed , _SoundGO);
		if (speed < 0.1f)
		{
			StopSound();
		}	
	}

	private void StopSound()
	{
		AudioManager.SetRTPCValue("Njorb_Speed", 0f, _SoundGO);
		_soundSpeed = 0f;
	}

	private void AirTimeGroundedCheck()
	{
		if(!_airTimeTimerRunning)
		{
			_airTimeTimerRunning = true;
			StartCoroutine(AirTimeTimer());
		}
	}

	IEnumerator AirTimeTimer()
	{
		if(_moveSoundPlaying)
		{
			AudioManager.PostEvent("Stop_Njorb_Move", _SoundGO);
			_moveSoundPlaying = false;
		}

		float timer = 0;
		while(!Grounded)
		{
			timer += Time.deltaTime;
			// if(timer > 5f)
			// {
			//     float timeToGround;
			//     PhysicsExtension.BezierCast(transform.position, new Vector3(0, -1, 0) * _GravityForce, _rb.velocity, 1, 1500f, out timeToGround);
			//     if(timeToGround > _minTimeLeft)
			//     {
			//         //play the sounds
			//     }
			//     break;
			// }
			yield return null;
		}

		float val = Mathf.Clamp(timer / 3f, 0, 1);

		AudioManager.SetRTPCValue("Njorb_Intensity", _LandCurve.Evaluate(val), _SoundGO);
		AudioManager.PostEvent("Play_Njorb_Land", _SoundGO);
		_moveSoundPlaying = true;

		_airTimeTimerRunning = false;
	}

	public void SetDriftSoundValue(float val)
	{
		AudioManager.SetRTPCValue("Njorb_Skid", Mathf.Clamp(val, 0, 1), _SoundGO);
	}
}
