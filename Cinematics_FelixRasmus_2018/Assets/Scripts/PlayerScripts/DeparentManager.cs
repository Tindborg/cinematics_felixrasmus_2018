﻿using System.Collections.Generic;
using UnityEngine;

public class DeparentManager : MonoBehaviour 
{

	[SerializeField]
	private List<GameObject> _DeparentAndDelete;

	[SerializeField]
	private List<GameObject> _DeparentOnly;

	[SerializeField]
	private List<GameObject> _DeleteOnPlayerDisconnect;

	private void Start()
	{
		foreach (GameObject go in _DeparentAndDelete)
		{
			go.transform.parent = null;
		}
		
		foreach (GameObject go in _DeparentOnly)
		{
			go.transform.parent = null;
		}
	}

	private void OnDisable()
	{
		foreach (GameObject go in _DeparentAndDelete)
		{
			if(go != null)
			{
				Destroy(go);
			}
		}

		foreach (GameObject go in _DeleteOnPlayerDisconnect)
		{
			if(go != null)
			{
				Destroy(go);
			}
		}
	}
	
	public void AddToDeleteList(GameObject go)
	{
		_DeleteOnPlayerDisconnect.Add(go);
	}
}
