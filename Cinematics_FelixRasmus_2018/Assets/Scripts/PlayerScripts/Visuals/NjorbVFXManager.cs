﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NjorbVFXManager : MonoBehaviour
{

    #region Serialized

        [Header("References")]

        [SerializeField]
        private Transform _DriftMasterContainer;

        [SerializeField]
        private ParticleSystem[] _DriftParticles;

        [SerializeField]
        private GameObject _DriftFlareComponents;

        [SerializeField]
        private Light _DriftPointLight;

        //

        [Space]

        [SerializeField]
        private ParticleSystem _PSTransformToBall;
        [SerializeField]
        private ParticleSystem _PSTransformToLegs;

        [SerializeField]
        private ParticleSystem _PSDustWalkR;
        [SerializeField]
        private ParticleSystem _PSDustWalkL;

        [SerializeField]
        private GameObject _ShockwaveBoost;
        [SerializeField]
        private GameObject _ShockwaveSlowdown;
        [SerializeField]
        private GameObject _ShockwaveGrappleRelease;

        [SerializeField]
        private ParticleSystem _PSBoost;

        //

        [Space]
        [Header("Values")]

        [SerializeField]
        private float _DriftLightIntensityMin = 1.4f;
        [SerializeField]
        private float _DriftLightIntensityMax = 2.8f;

        //

        [Space]
        [Header("Color Propagation")]

        [SerializeField]
        private ColorSpecificPropagator[] _Propagators;

    #endregion Serialized

    #region Variables

        private IEnumerator _coroutine_DriftLight;

    #endregion Variables

    #region Lifecycle

        private void Awake()
        {
            _coroutine_DriftLight = Coroutine_DriftLight();
        }

    #endregion Lifecycle

    #region Coroutines

        private IEnumerator Coroutine_DriftLight()
        {
            while (true)
            {
                _DriftPointLight.intensity = Random.Range(_DriftLightIntensityMin, _DriftLightIntensityMax);
                
                yield return null;
            }
        }

    #endregion Coroutines

    #region Functions

        public void PropagateAll(int colorId)
        {
            for (int i = 0; i < _Propagators.Length; i ++)
            {
                _Propagators[i].Propagate(colorId);
            }
        }

        /// Continuous effects

        public void StartDrift(bool driftingRelativeRight)
        {
            _DriftMasterContainer.localPosition = new Vector3(driftingRelativeRight ? 0.3f : -0.3f, 0, 0);

            _DriftParticles[0].Play();
            _DriftParticles[1].Play();
            _DriftParticles[2].Play();
            _DriftParticles[3].Play();

            _DriftFlareComponents.SetActive(true);

            _DriftPointLight.enabled = true;
            StartCoroutine(_coroutine_DriftLight);
        }

        public void StartDustWalk()
        {
            _PSDustWalkR.Play();
            _PSDustWalkL.Play();
        }


        /// One-offs

        public void PlayTransformToBall()
        {
            _PSTransformToBall.Play();
        }

        public void PlayTransformToLegs()
        {
            _PSTransformToLegs.Play();
        }

        public void PlayShockwaveBoost()
        {
            _PSBoost.Play();
            
            if (_ShockwaveBoost.activeSelf)
            {
                _ShockwaveBoost.SetActive(true);
            } else {
                _ShockwaveBoost.SetActive(false);
                _ShockwaveBoost.SetActive(true);
            }
        }

        public void PlayShockwaveSlowdown()
        {
            if (_ShockwaveSlowdown.activeSelf)
            {
                _ShockwaveSlowdown.SetActive(true);
            } else {
                _ShockwaveSlowdown.SetActive(false);
                _ShockwaveSlowdown.SetActive(true);
            }
        }

        public void PlayShockwaveGrappleRelease()
        {
            if (_ShockwaveGrappleRelease.activeSelf)
            {
                _ShockwaveGrappleRelease.SetActive(true);
            } else {
                _ShockwaveGrappleRelease.SetActive(false);
                _ShockwaveGrappleRelease.SetActive(true);
            }
        }


        /// Stop continuous

        public void StopDrift()
        {
            _DriftParticles[0].Stop();
            _DriftParticles[1].Stop();
            _DriftParticles[2].Stop();
            _DriftParticles[3].Stop();

            _DriftFlareComponents.SetActive(false);

            _DriftPointLight.enabled = false;
            StopCoroutine(_coroutine_DriftLight);
        }

        public void StopDustWalk()
        {
            _PSDustWalkR.Stop();
            _PSDustWalkL.Stop();
        }

    #endregion Functions

}
