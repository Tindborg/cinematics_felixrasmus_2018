﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunTimeShaderValueTest : MonoBehaviour 
{

	[SerializeField]
	private float _value;

	private Material[] _mat;

	public void StoreMaterial()
	{
		_mat = GetComponent<MeshRenderer>().materials;	
	}

	void Update () 
	{
		if(_mat == null) return;
		foreach (Material mat in _mat)
		{		
			mat.SetFloat("_StepTest", _value);
		}
	}
}
